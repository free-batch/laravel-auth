{{-- <x-backend.layouts.master> --}}
        <form action="{{ route('roles.store')}}" method="POST">
            @csrf
            <div class="d-flex align-items-center">
                <label for="name">Role Name: </label>
                <input type="text" name="name">
            </div>
            <div class="d-flex align-items-center">
                <label for="description">Description: </label>
                <textarea id="myeditorinstance" name="description" placeholder="Write a description, if you want-"></textarea>
            </div>
            <div class="d-flex align-items-center mt-2">
                <label for="status">Status</label>
                {{-- <input type="text" name="status"> --}}
                <input type="radio" id="active" name="status" value="1">
                <label for="active">Active</label><br>
                <input type="radio" id="inactive" name="status" value="0">
                <label for="inactive">Inactive</label><br>
            </div>
            <button type="submit">Save</button>
        </form>
        @push('js')
        {{-- <script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script> --}}
        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.3.0/tinymce.min.js" integrity="sha512-CpqBk+ddDL2iYxwLkBkqiL9HywSfSfVQdkZThgvEryhQXnGlrrp9foNf6K9hDM+QrNUyT9ElgRoKJLSnsLujow==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.7.0/tinymce.min.js" integrity="sha512-kGk8SWqEKL++Kd6+uNcBT7B8Lne94LjGEMqPS6rpDpeglJf3xpczBSSCmhSEmXfHTnQ7inRXXxKob4ZuJy3WSQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
          tinymce.init({
            selector: 'textarea#myeditorinstance', // Replace this CSS selector to match the placeholder element for TinyMCE
            plugins: 'code table lists',
            toolbar: 'undo redo | formatselect| bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table'
          });
        </script>
    
        @endpush
{{-- </x-backend.layouts.master> --}}