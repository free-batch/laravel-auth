{{-- <x-backend.layouts.master> --}}
    <form action="{{ route('roles.update', $role->id)}}" method="POST">
        @CSRF
        @method('PATCH')
        <label for="name">Role Name</label>
        <input type="text" name="name" value="{{$role->name}}">
        <div class="d-flex align-items-center">
            <label for="description">Description: </label>
            <textarea id="myeditorinstance" name="description" placeholder="Write a description, if you want-">{{$role->description}}</textarea>
        </div>
        <div class="d-flex align-items-center mt-2">
            <label for="status">Status</label>
            {{-- <input type="text" name="status"> --}}
            <input type="radio" id="active" name="status" value="1">
            <label for="active">Active</label><br>
            <input type="radio" id="inactive" name="status" value="0">
            <label for="inactive">Inactive</label><br>
        </div>
        <button type="submit">Save</button>
    </form>
{{-- </x-backend.layouts.master> --}}