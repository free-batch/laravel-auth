<?php

namespace App\Policies;

use App\Models\User;

class RolePolicy
{
    /**
     * Create a new policy instance.
     */
    public function __construct()
    {
        
    }

    public function create(User $user)
    {
        if ($user->role_id == 1) {
            return true;
        }

        return false;
    }
}
