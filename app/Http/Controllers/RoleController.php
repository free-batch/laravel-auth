<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::get();
        return view('backend.roles.index', compact('roles'));
    }

    public function create()
    {
        if(auth()->user()->role_id == 2){
            return view('components.unauthorized');
        }
        return view('backend.roles.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        Role::create([
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
        ]);
        return redirect()->route('roles.index');
    }

    public function show(Role $role)
    {
        return view('backend.roles.show', compact('role'));
    }

    public function edit(Role $role)
    {
        return view('backend.roles.edit', compact('role'));
    }

    public function update(Request $request, Role $role)
    {
        $role->update([
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
        ]);
        return redirect()->route('roles.index');
    }

    public function destroy(Role $role)
    {
        //
    }
}
